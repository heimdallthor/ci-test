# 0.5.0 (2020-09-15)
## added
* this is the second version created by ci ( https://gitlab.com/heimdallthor/ci-test/-/merge_requests/7)
# 0.4.0 (2020-09-15)
## added
* this is the first version to be created and build by ci
# 0.3.0 (2020-09-14)
## fixed
* add one change log item
# 0.3.0 (2020-09-14)
## fixed
* add one change log item
